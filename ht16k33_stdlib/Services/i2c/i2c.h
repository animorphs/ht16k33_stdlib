#ifndef __I2C_H
#define __I2C_H

#include "stm32f0xx.h"


#define I2C_SHORT_TIMEOUT         ((DWORD)0x1000)
#define I2C_LONG_TIMEOUT          ((DWORD)(10 * I2C_SHORT_TIMEOUT)) 

// I2Cx = I2C1 hoac I2C2

void I2C1_Config(void);
uint8_t I2C_WriteMem(I2C_TypeDef* I2Cx, uint16_t DeviceAddr, uint8_t nRegister, uint8_t* pData, uint8_t nLen);
uint8_t I2C_ReadByte(I2C_TypeDef* I2Cx, uint8_t byDeviceAddr, uint8_t byRegister);
uint16_t I2C_ReadWord(I2C_TypeDef* I2Cx, uint8_t byteDeviceAddr, uint8_t byRegister);
void I2C_ReleaseBus(void);
uint8_t I2C_WriteBuffer(I2C_TypeDef* I2Cx, uint16_t DeviceAddr, uint8_t* pData, uint8_t nLen);
#endif 

