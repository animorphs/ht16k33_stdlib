/********************************************************************************  
 
Product: DIMMER
 
Module: i2c.c
 
Version: 1.0 
 
Author: THANHNK
 
Created: 08-2016 
 
Modified:  
  <Name> 
  <Date> 
  <Change> 
 
Released: 
 
Description: Including functions for I2C
 
Note: <Note> 
 
********************************************************************************/

#include "i2c.h"
#include "stm32f0xx_i2c.h"
#include "stm32f0xx_gpio.h"

#define SDA_PIN         GPIO_Pin_9
#define SCL_PIN         GPIO_Pin_8
#define GPIO_I2C_PORT   GPIOB

static void i2c_release_bus_delay(void);


DWORD   I2C_TimeOut = I2C_LONG_TIMEOUT;

/* API */
VOID I2C1_Config(VOID)
{  
    GPIO_InitTypeDef  GPIO_InitStructure;
    
    /* Configure the I2C clock source. The clock is derived from the HSI */
    RCC_I2CCLKConfig(RCC_I2C1CLK_HSI);
    
    /* sEE_I2C_SCL_GPIO_CLK and sEE_I2C_SDA_GPIO_CLK Periph clock enable */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    
    /* sEE_I2C Periph clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
    
    /* Connect PXx to I2C_SCL*/
    GPIO_PinAFConfig(GPIO_I2C_PORT, GPIO_PinSource8, GPIO_AF_1);
    
    /* Connect PXx to I2C_SDA*/
    GPIO_PinAFConfig(GPIO_I2C_PORT, GPIO_PinSource9, GPIO_AF_1);
    
    /* GPIO configuration */  
    /* Configure sEE_I2C pins: SCL */
    GPIO_InitStructure.GPIO_Pin = SCL_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIO_I2C_PORT, &GPIO_InitStructure);
    
    /* Configure sEE_I2C pins: SDA */
    GPIO_InitStructure.GPIO_Pin = SDA_PIN;
    GPIO_Init(GPIO_I2C_PORT, &GPIO_InitStructure);
    
    
    I2C_InitTypeDef  I2C_InitStructure;
    
    I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
    I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
    I2C_InitStructure.I2C_DigitalFilter = 0x00;
    I2C_InitStructure.I2C_OwnAddress1 = 0x00;
    I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
    I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2C_InitStructure.I2C_Timing = 0x00201D2B;//0x40B22536;
    
    I2C_Init(I2C1, &I2C_InitStructure);
    
    I2C_Cmd(I2C1, ENABLE);
}

/*------------------------------------------------------------------------------- 
Function: BOOL I2C_WriteMem(I2C_TypeDef* I2Cx, uint8_t DeviceAddr, uint8_t nRegister, uint8_t* pData, uint8_t nLen)
 
Purpose: Write a uint8_t to I2C
 
Parameters: I2Cx, DeviceAddr, nRegister, byData
 
Return: TRUE
 
Comments:  None
 
Modified: 
  <Modified by> 
  <Date> 
  <Change> 
--------------------------------------------------------------------------------*/
uint8_t I2C_WriteMem(I2C_TypeDef* I2Cx, uint16_t DeviceAddr, uint8_t nRegister, uint8_t* pData, uint8_t nLen)
{  
    I2C_TimeOut = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(I2Cx,I2C_ISR_BUSY) != RESET)
    { 
        if((I2C_TimeOut--) == 0)            
            return FALSE;
    }
    I2C_TransferHandling(I2Cx,DeviceAddr, 1,I2C_Reload_Mode,I2C_Generate_Start_Write);
    
    I2C_TimeOut = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(I2Cx,I2C_ISR_TXIS) == RESET)
    {
        if((I2C_TimeOut--) == 0)            
            return FALSE;
    }
    I2C_SendData(I2Cx, (uint8_t)nRegister);
    
    I2C_TimeOut = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(I2Cx,I2C_ISR_TCR)==RESET)
    {
        if((I2C_TimeOut--) == 0)            
            return FALSE;
    }
    I2C_TransferHandling(I2Cx,DeviceAddr,nLen,I2C_AutoEnd_Mode,I2C_No_StartStop);
    
    while (nLen > 0U)
    {
        /* Wait until TXIS flag is set */
        I2C_TimeOut = I2C_LONG_TIMEOUT;
        while (I2C_GetFlagStatus(I2Cx,I2C_ISR_TXIS) == RESET)
        {
            if((I2C_TimeOut--) == 0)            
                return FALSE;
        }
        /* Write data to TXDR */
        I2C_SendData(I2Cx, *pData);
        /* Increment Buffer pointer */
        pData++;
        nLen--;

        if (nLen == 0U)
        {
            /* Wait until TCR flag is set */
            I2C_TimeOut = I2C_LONG_TIMEOUT;
            while (I2C_GetFlagStatus(I2Cx,I2C_ISR_TCR)==RESET)
            {
                if((I2C_TimeOut--) == 0)            
                    return FALSE;
            }
            I2C_TransferHandling(I2Cx, DeviceAddr, nLen, I2C_AutoEnd_Mode, I2C_No_StartStop);
            I2C_TimeOut = I2C_LONG_TIMEOUT;
            while (I2C_GetFlagStatus(I2Cx,I2C_ISR_STOPF) == RESET )
            {
                if((I2C_TimeOut--) == 0)            
                    return FALSE;
            }
            I2C_ClearFlag(I2Cx,I2C_ICR_STOPCF);
        }
    } 
    
    return TRUE; 
}

/*------------------------------------------------------------------------------- 
Function: uint8_t I2C_ReadByte(I2C_TypeDef* I2Cx, uint8_t DeviceAddr, uint8_t nRegister)
 
Purpose: Read a uint8_t from slave
 
Parameters: I2Cx, DeviceAddr, nRegister
 
Return: byResult
 
Comments:  None
 
Modified: 
  <Modified by> 
  <Date> 
  <Change> 
--------------------------------------------------------------------------------*/
uint8_t I2C_ReadByte(I2C_TypeDef* I2Cx, uint8_t DeviceAddr, uint8_t nRegister)
{
    uint8_t byResult;
   
    I2C_TimeOut = I2C_SHORT_TIMEOUT ; 
    while(I2C_GetFlagStatus(I2Cx, I2C_ISR_BUSY) != RESET)
    {
        if((I2C_TimeOut--) == 0)            
            return 0xFF;
    }
    I2C_TransferHandling(I2Cx,DeviceAddr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
    I2C_TimeOut = I2C_SHORT_TIMEOUT; 
    while(I2C_GetFlagStatus(I2Cx, I2C_ISR_TXIS) == RESET)
    {
        if((I2C_TimeOut--) == 0) 
            return 0xFF;
    }
        
    /* Send Register address */
    I2C_SendData(I2Cx,(uint8_t) nRegister);
    I2C_TimeOut = I2C_SHORT_TIMEOUT;
    while(I2C_GetFlagStatus(I2Cx, I2C_ISR_TC) == RESET)
    {
        if((I2C_TimeOut--) == 0) 
            return 0xFF;
    }  
    
    /* Configure slave address, nbytes, reload, end mode and start or stop generation */
    I2C_TransferHandling(I2Cx, DeviceAddr, 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
    I2C_TimeOut = I2C_SHORT_TIMEOUT ; 
    while (I2C_GetFlagStatus (I2Cx,I2C_ISR_RXNE) == RESET)
    {
        if ((I2C_TimeOut--) == 0 ) 
            return 0xFF;
    }
    byResult = I2C_ReceiveData(I2Cx);
    I2C_TimeOut = I2C_SHORT_TIMEOUT ; 
    while (I2C_GetFlagStatus(I2Cx,I2C_ISR_STOPF) == RESET)
    {
        if ((I2C_TimeOut--) == 0 ) 
            return 0xFF;
    }
    I2C_ClearFlag(I2Cx,I2C_ICR_STOPCF);
    return (uint8_t)byResult ; 
}

/*------------------------------------------------------------------------------- 
Function: uint16_t I2C_ReadWord(I2C_TypeDef* I2Cx, uint8_t DeviceAddr, uint8_t nRegister)
 
Purpose: Read a uint8_t from slave
 
Parameters: I2Cx, DeviceAddr, nRegister
 
Return: byResult
 
Comments:  None
 
Modified: 
  <Modified by> 
  <Date> 
  <Change> 
--------------------------------------------------------------------------------*/
uint16_t I2C_ReadWord(I2C_TypeDef* I2Cx, uint8_t DeviceAddr, uint8_t nRegister)
{
    uint8_t byTmp[2] = {0,0}, byDataNum = 0;
    uint16_t wValue;
    
    I2C_TimeOut = I2C_LONG_TIMEOUT ; 
    while(I2C_GetFlagStatus(I2Cx, I2C_ISR_BUSY) != RESET)
    {
        if((I2C_TimeOut--) == 0)             
            return 0xFF;
    }
    I2C_TransferHandling(I2Cx,DeviceAddr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
    I2C_TimeOut = I2C_LONG_TIMEOUT; 
    while(I2C_GetFlagStatus(I2Cx, I2C_ISR_TXIS) == RESET)
    {
        if((I2C_TimeOut--) == 0) return 
            0xFF;
    }
        
    /* Send Register address */
    I2C_SendData(I2Cx,(uint8_t) nRegister);
    I2C_TimeOut = I2C_LONG_TIMEOUT;
    while(I2C_GetFlagStatus(I2Cx, I2C_ISR_TC) == RESET)
    {
        if((I2C_TimeOut--) == 0) return 
            0xFF;
    }  
    
    /* Configure slave address, nbytes, reload, end mode and start or stop generation */
    I2C_TransferHandling(I2Cx, DeviceAddr, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
    I2C_TimeOut = I2C_LONG_TIMEOUT ; 
    byDataNum = 0;
    while (byDataNum != 2)
    {
        while (I2C_GetFlagStatus (I2Cx,I2C_ISR_RXNE) == RESET)
        {
            if ((I2C_TimeOut--) == 0) 
                return 0xFF;
        }
        byTmp[byDataNum] = I2C_ReceiveData(I2Cx);
        byDataNum++;
    }

    I2C_TimeOut = I2C_LONG_TIMEOUT ; 
    while (I2C_GetFlagStatus(I2Cx,I2C_ISR_STOPF) == RESET)
    {
        if ((I2C_TimeOut--) == 0 ) 
            return 0xFF;
    }
    I2C_ClearFlag(I2Cx,I2C_ICR_STOPCF);
    wValue = (uint16_t)(byTmp[0] << 8);
    wValue |= byTmp[1];
    return (uint16_t)wValue ; 
}

/* Delay to create pulse*/
static void i2c_release_bus_delay(void)
{
    uint32_t i = 0;
    for (i = 0; i < 48; i++)
    {
        __NOP();
    }
}
/**/
void I2C_ReleaseBus(void)
{
    uint8_t i = 0;

    GPIO_InitTypeDef GPIO_InitStructure;
    /* Config pin mux as gpio */
    /* Configure for RF_SDN, RF_IRQ */
    GPIO_InitStructure.GPIO_Pin = SDA_PIN | SCL_PIN; //SDN
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIO_I2C_PORT, &GPIO_InitStructure);

    /* Drive SDA low first to simulate a start */
    GPIO_ResetBits(GPIO_I2C_PORT, SDA_PIN);
    i2c_release_bus_delay();

    /* Send 9 pulses on SCL and keep SDA high */
    for (i = 0; i < 9; i++)
    {
        GPIO_ResetBits(GPIO_I2C_PORT, SCL_PIN);
        i2c_release_bus_delay();

        GPIO_SetBits(GPIO_I2C_PORT, SDA_PIN);
        i2c_release_bus_delay();

        GPIO_SetBits(GPIO_I2C_PORT, SCL_PIN);
        i2c_release_bus_delay();
        i2c_release_bus_delay();
    }

    /* Send stop */
    GPIO_ResetBits(GPIO_I2C_PORT, SCL_PIN);
    i2c_release_bus_delay();

    GPIO_ResetBits(GPIO_I2C_PORT, SDA_PIN);
    i2c_release_bus_delay();

    GPIO_SetBits(GPIO_I2C_PORT, SCL_PIN);
    i2c_release_bus_delay();

    GPIO_SetBits(GPIO_I2C_PORT, SDA_PIN);
    i2c_release_bus_delay();
    I2C1_Config();
}

/*------------------------------------------------------------------------------- 
Function: BOOL I2C_WriteBuffer(I2C_TypeDef* I2Cx, uint8_t DeviceAddr, uint8_t* pData, uint8_t nLen)
 
Purpose: Write a BUFFER to I2C
 
Parameters: I2Cx, DeviceAddr, pData, nLen
 
Return: TRUE
 
Comments:  None
 
Modified: 
  <Modified by> 
  <Date> 
  <Change> 
--------------------------------------------------------------------------------*/
uint8_t I2C_WriteBuffer(I2C_TypeDef* I2Cx, uint16_t DeviceAddr, uint8_t* pData, uint8_t nLen)
{  
    I2C_TimeOut = I2C_LONG_TIMEOUT;
    while (I2C_GetFlagStatus(I2Cx,I2C_ISR_BUSY) != RESET)
    { 
        if((I2C_TimeOut--) == 0)            
            return FALSE;
    }
    I2C_TransferHandling(I2Cx,DeviceAddr,nLen,I2C_AutoEnd_Mode,I2C_Generate_Start_Write);
    
    while (nLen > 0U)
    {
        /* Wait until TXIS flag is set */
        I2C_TimeOut = I2C_LONG_TIMEOUT;
        while (I2C_GetFlagStatus(I2Cx,I2C_ISR_TXIS) == RESET)
        {
            if((I2C_TimeOut--) == 0)            
                return FALSE;
        }
        /* Write data to TXDR */
        I2C_SendData(I2Cx, *pData);
        /* Increment Buffer pointer */
        pData++;
        nLen--;

        if (nLen == 0U)
        {
            /* Wait until TCR flag is set */
            I2C_TimeOut = I2C_LONG_TIMEOUT;
            while (I2C_GetFlagStatus(I2Cx,I2C_ISR_TCR)==RESET)
            {
                if((I2C_TimeOut--) == 0)            
                    return FALSE;
            }
            I2C_TransferHandling(I2Cx, DeviceAddr, nLen, I2C_AutoEnd_Mode, I2C_No_StartStop);
            I2C_TimeOut = I2C_LONG_TIMEOUT;
            while (I2C_GetFlagStatus(I2Cx,I2C_ISR_STOPF) == RESET )
            {
                if((I2C_TimeOut--) == 0)            
                    return FALSE;
            }
            I2C_ClearFlag(I2Cx,I2C_ICR_STOPCF);
        }
    }    
    return TRUE; 
}
