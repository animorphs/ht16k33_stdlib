#include "stm32f0xx_it.h"
#include "system.h"
__IO uint32_t tickcount = 0;
/******************************************************************************/
/*            Cortex-M0 Processor Exceptions Handlers                         */
/******************************************************************************/


void NMI_Handler(void)
{
}

void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  NVIC_SystemReset();
  while (1)
  {
  }
}

void SVC_Handler(void)
{
}

void PendSV_Handler(void)
{
}


VOID SysTick_Handler(VOID)
{
  tickcount++;
}

/******************************************************************************/
/*                 STM32F0xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f0xx.s).                                               */
/******************************************************************************/


VOID USART1_IRQHandler(VOID)
{

}
VOID USART2_IRQHandler(VOID)
{
 
}
        
void TIM3_IRQHandler(void)
{   
}
void TIM14_IRQHandler(void)
{   
}




/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
