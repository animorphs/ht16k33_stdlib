/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "system.h"
#include "i2c.h"
#include "ht16k33.h"

void ht16k33_Init(void)
{ 
  uint8_t setup_param = 0x21;
  
  I2C_WriteBuffer(I2C1, _i2c_addr, &setup_param, 1);
  // set blink off + brightness all the way up
  ht16k33_setBlink(HT16K33_BLINK_OFF);
  ht16k33_setBrightness(15);
  }

void ht16k33_WriteData(uint8_t* pData, uint8_t len)
{
  I2C_WriteMem(I2C1, _i2c_addr, HT16K33_CMD_RAM, pData, len);
}

void ht16k33_setBrightness(uint8_t brightness)
{
    // constrain the brightness to a 4-bit number (0–15)
    brightness = brightness & 0x0F;
    brightness = HT16K33_CMD_DIMMING | brightness;
    I2C_WriteBuffer(I2C1, _i2c_addr, &brightness, 1);
}

void ht16k33_setBlink(uint8_t blink)
{
  blink &= 0x60;
  blink = HT16K33_CMD_SETUP | HT16K33_DISPLAY_ON | blink;
  I2C_WriteBuffer(I2C1, _i2c_addr, &blink, 1);
}