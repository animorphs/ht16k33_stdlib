/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HT16K33_H
#define __HT16K33_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx.h"

  // different commands
  #define HT16K33_CMD_RAM     0x00
  #define HT16K33_CMD_KEYS    0x40
  #define HT16K33_CMD_SETUP   0x80
  #define HT16K33_CMD_ROWINT  0xA0
  #define HT16K33_CMD_DIMMING 0xE0
  
  // other options
  #define HT16K33_DISPLAY_OFF 0x00
  #define HT16K33_DISPLAY_ON  0x01
  #define HT16K33_BLINK_OFF   0x00
  #define HT16K33_BLINK_1HZ   0x02
  #define HT16K33_BLINK_2HZ   0x04
  #define HT16K33_BLINK_0HZ5  0x06

  #define _i2c_addr (0x70<<1)

void ht16k33_Init(void);
void ht16k33_WriteData(uint8_t* pData, uint8_t len);
void ht16k33_setBrightness(uint8_t brightness);
void ht16k33_setBlink(uint8_t blink);

#ifdef __cplusplus
}
#endif

#endif /* __HT16K33_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
