#ifndef __ADRESS_H__
#define __ADRESS_H__

#include "system.h"

/*************** Area Save Firmware *******************/
#define ADDR_UPDATE_FIRMWARE		0x8800
#define PAGE_UPDATE_FIRMWARE		34 // PAGE: 34...58

/*************** BOOTLOADER ADDR ****************/
#define ADDR_BOOTLOADER 			0xEC00 // Page 59.60.61

/************ Area Main Program ****************/
#define IAP_ADDR_OFFSET 	0x00000
#define PAGE_OF_IAP			0

#define NUMBER_OF_PAGE_UP_FIRMWARE	25 // 25page = 25kbyte	 


/******* Luu lai TimeTable *******/
#define ADDR_SAVE_TIMETABLE			0x8400 
#define FLASH_PAGE_TIMETABLE		33	 //PAGE: 33
#define NUMBER_OF_PAGE_TIMETABLE	1

#endif //