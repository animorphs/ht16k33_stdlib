/**
  * @file    DIMMER/main.c 
  * @author  ThanhNK
  * @version V1.0.0
  * @date    09/08/2016
  * @brief   Main program body
  */
/* Includes ------------------------------------------------------------------*/
#include "system.h"

extern __IO uint32_t tickcount;
uint8_t value_1[16];
uint8_t value_0[16];
/* Private function prototypes -----------------------------------------------*/
void HAL_Delay(uint32_t Delay);
VOID SysTickConfig(VOID);
VOID InitApp(VOID);
VOID GPIO_Configuration(VOID);
/*************Init Device*****************/
VOID InitApp(VOID)
{
    for(uint8_t i = 0; i < 16; i++)
    {
        value_0[i] = 0x00;
        value_1[i] = 0xff;
    }

    GPIO_Configuration();
    SysTickConfig();
    I2C1_Config();
    ht16k33_Init();
    HAL_Delay(500);
}

int main(void)
{     
    InitApp(); 
    while (1)
    {
        ht16k33_WriteData(value_1, 16);
        HAL_Delay(1000);
        ht16k33_WriteData(value_0, 16);
        HAL_Delay(1000);
    }  
}

/****************Config SysTick******************/
VOID SysTickConfig (VOID)
{
    while(SysTick_Config(SystemCoreClock / 1000));
}

/******************Config GPIO********************/
VOID GPIO_Configuration(VOID)
{
    /* Periph clock enable */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
}


void HAL_Delay(uint32_t Delay)
{
  uint32_t tickstart = tickcount;
  uint32_t wait = Delay;
  
  
  while((tickcount - tickstart) < wait)
  {
  }
}

/*****************END OF FILE***********************/

