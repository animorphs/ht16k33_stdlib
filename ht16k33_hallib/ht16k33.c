/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal_i2c.h"
#include "ht16k33.h"

extern I2C_HandleTypeDef hi2c1;

void ht16k33_Init(void)
{ 
  uint8_t setup_param = 0x21;
  
  while(HAL_I2C_Master_Transmit(&hi2c1, _i2c_addr, &setup_param, 1 , 10000) != HAL_OK)
  {
    /* Error_Handler() function is called when Timeout error occurs.
       When Acknowledge failure occurs (Slave don't acknowledge its address)
       Master restarts communication */
    if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
    {
      Error_Handler();
    }
  }
  // set blink off + brightness all the way up
  ht16k33_setBlink(HT16K33_BLINK_OFF);
  ht16k33_setBrightness(15);
  }

void ht16k33_WriteData(uint8_t* pData, uint8_t len)
{
  while(HAL_I2C_Mem_Write(&hi2c1, _i2c_addr, HT16K33_CMD_RAM, 1, pData, len, 10000)!= HAL_OK)
  {
    /* Error_Handler() function is called when Timeout error occurs.
       When Acknowledge failure occurs (Slave don't acknowledge its address)
       Master restarts communication */
    if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
    {
      Error_Handler();
    }
  }
}

void ht16k33_setBrightness(uint8_t brightness)
{
    // constrain the brightness to a 4-bit number (0–15)
    brightness = brightness & 0x0F;
    brightness = HT16K33_CMD_DIMMING | brightness;
    while(HAL_I2C_Master_Transmit(&hi2c1, _i2c_addr, &brightness, 1 , 10000)!= HAL_OK)
    {
      /* Error_Handler() function is called when Timeout error occurs.
        When Acknowledge failure occurs (Slave don't acknowledge its address)
        Master restarts communication */
      if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
      {
        Error_Handler();
      }
    }
}

void ht16k33_setBlink(uint8_t blink)
{
  blink &= 0x60;
  blink = HT16K33_CMD_SETUP | HT16K33_DISPLAY_ON | blink;
  while(HAL_I2C_Master_Transmit(&hi2c1, _i2c_addr, &blink, 1 , 10000)!= HAL_OK)
  {
    /* Error_Handler() function is called when Timeout error occurs.
       When Acknowledge failure occurs (Slave don't acknowledge its address)
       Master restarts communication */
    if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
    {
      Error_Handler();
    }
  }
}